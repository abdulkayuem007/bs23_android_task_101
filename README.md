
  

# BS23_Android_Task_101

  

  

I have developed a simple Android application which shows the most starred github repositories by searching

  

with the keyword "Android".

  

  

## Getting started

  

**Steps of work:**

  

  

- Find out the github repo's reading the docs: [here](https://api.github.com/search/repositories?q=android&sort=stars&order=desc&per_page=10)

  

- Test API's in Postman

  

- Scratch the design in notebook.

  

- Choose the tech stacks used for this projects.

  

- Architecture I used:

  

- ![MVVM](https://gitlab.com/abdulkayuem007/bs23_android_task_101/-/raw/master/photos/0_PKo4mQsOOGUqPlVp.png)

  

- Tech stack I used:

- *Kotlin,*

- *Retrofit,*

- *MVVM,*

- *SharedPreferences,*

- *ViewBinding*


My project flow:

![enter image description here](https://gitlab.com/abdulkayuem007/bs23_android_task_101/-/raw/main/photos/project_structure.PNG)
  

  

My Home page of the projects:

  

  

![enter image description here](https://gitlab.com/abdulkayuem007/bs23_android_task_101/-/raw/master/photos/dd0e4ed0-37cd-4edd-84ad-24f1842c15df.jpg)

  
  
  

Details page:

  
  

![enter image description here](https://gitlab.com/abdulkayuem007/bs23_android_task_101/-/raw/main/photos/6465f4b7-2244-44de-95c3-522b49a8c93c.jpg)

  
  
  
  

**Future task**:

- Save Data locally, Also will use coroutine,

  

**Missing Work :**

Sorting Icon
