package com.example.githubtopapps.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.githubtopapps.data.common.RequestCompleteListener
import com.example.githubtopapps.data.remote.GitHubDataInfoModel
import com.example.githubtopapps.data.remote.GithubDataInfoModelShowModelImpl
import com.example.githubtopapps.data.remote.response.GithubResponse

class MainViewModel : ViewModel() {

    val githubDataInfoLiveData = MutableLiveData<GithubResponse>()
    val githubDataInfoFailureLiveData = MutableLiveData<String>()
    val progressBarLiveData = MutableLiveData<Boolean>()


    fun getGithubInfo(
        topics: String, sort: String, order: String, per_page: String,
        model: GitHubDataInfoModel
    ) {
        progressBarLiveData.postValue(true)

        progressBarLiveData.postValue(true)

        model.getGitHubRepoInfo(topics,
            sort,
            order,
            per_page, object :
                RequestCompleteListener<GithubResponse> {
                override fun onRequestSuccess(data: GithubResponse) {


                    val githubData = GithubResponse(
                        totalCount = data.totalCount,
                        incompleteResults = data.incompleteResults,
                        items = data.items
                    )

                    progressBarLiveData.postValue(false)
                    githubDataInfoLiveData.postValue(githubData)
                }

                override fun onRequestFailed(errorMessage: String) {
                    progressBarLiveData.postValue(false)
                    githubDataInfoFailureLiveData.postValue(errorMessage)
                }
            })


    }

}