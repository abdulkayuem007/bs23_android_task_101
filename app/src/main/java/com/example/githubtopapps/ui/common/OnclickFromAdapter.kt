package com.example.githubtopapps.ui.common

import com.example.githubtopapps.data.remote.response.Items

interface OnclickFromAdapter {
    fun details(
        name: String,
        description: String,
        image: String,
        totalsStars: String,
        toString: String,
        homePage: String
    )

}