package com.example.githubtopapps.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.githubtopapps.R
import com.example.githubtopapps.databinding.ActivityDetailsBinding
import com.example.githubtopapps.databinding.ActivityMainBinding


class DetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityDetailsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setStatusBarColor()

        getDataAndSetData()



    }

    private fun setStatusBarColor() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.card_color)

    }

    private fun getDataAndSetData() {
        val name = intent.getStringExtra("name")
        val description = intent.getStringExtra("description")
        val image = intent.getStringExtra("image")
        val totalsStars = intent.getStringExtra("totalsStars")
        val repoUrl = intent.getStringExtra("repoUrl")
        val homepage = intent.getStringExtra("homePage")




        val transformation = MultiTransformation(CenterCrop(), RoundedCorners(10))
        Glide.with(this)
            .load(image)
            .transform(transformation)
            .into(binding.imageRepo)

        binding.projectName.text = name
        binding.repoDesc.text = description
        binding.starOfRepo.text = totalsStars
        binding.homepage.text = homepage
        binding.repoUrl.text = repoUrl


    }
}