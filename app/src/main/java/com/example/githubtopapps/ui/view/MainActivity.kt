package com.example.githubtopapps.ui.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubtopapps.R
import com.example.githubtopapps.data.remote.GitHubDataInfoModel
import com.example.githubtopapps.data.remote.GithubDataInfoModelShowModelImpl
import com.example.githubtopapps.data.remote.response.Items
import com.example.githubtopapps.databinding.ActivityMainBinding
import com.example.githubtopapps.ui.adapter.MainAdapter
import com.example.githubtopapps.ui.common.OnclickFromAdapter
import com.example.githubtopapps.ui.viewmodel.MainViewModel
import com.google.gson.Gson


class MainActivity : AppCompatActivity() , OnclickFromAdapter{

    private lateinit var model: GitHubDataInfoModel
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter
    private lateinit var binding: ActivityMainBinding
    private lateinit var githubResponses: List<Items>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setStatusBarColor()

        setUpViewModel()


        setUpUI()

        showData()

    }

    private fun setUpViewModel() {

        model = GithubDataInfoModelShowModelImpl(applicationContext)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
    }

    private fun setUpUI() {
        binding.repoRV.layoutManager =  LinearLayoutManager(this)
        githubResponses = arrayListOf()
        adapter = MainAdapter(githubResponses,applicationContext,this)
    }

    private fun setStatusBarColor() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.card_color)
    }

    private fun showData() {
        viewModel.getGithubInfo(
            "android","stars","desc","10",model)


        viewModel.progressBarLiveData.observe(this, { isShowLoader ->
//            if (isShowLoader)
//                progressBar.visibility = View.VISIBLE
//            else
//                progressBar.visibility = View.GONE
        })



        viewModel.githubDataInfoLiveData.observe(this) { data ->
            Log.d("responseMain", Gson().toJson(data))

            githubResponses = data.items


            adapter.addData(githubResponses)

            binding.repoRV.adapter = adapter


        }
        viewModel.githubDataInfoFailureLiveData.observe(this) { data ->
            Log.d("responseMain", Gson().toJson(data))
        }


    }

    override fun details(
        name: String,
        description: String,
        image: String,
        totalsStars: String,
        repoUrl: String,
        homePage: String
    ) {
        val intent = Intent(this, DetailsActivity::class.java)

        intent.putExtra("name",name)
        intent.putExtra("description",description)
        intent.putExtra("image",image)
        intent.putExtra("totalsStars",totalsStars)
        intent.putExtra("repoUrl", repoUrl)
        intent.putExtra("homePage", homePage)

        startActivity(intent)
    }

}