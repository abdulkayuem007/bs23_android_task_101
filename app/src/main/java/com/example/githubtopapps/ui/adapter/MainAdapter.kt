package com.example.githubtopapps.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.githubtopapps.data.remote.response.Items
import com.example.githubtopapps.R
import com.example.githubtopapps.ui.common.OnclickFromAdapter


class MainAdapter(
    private var repoInfo: List<Items>,
    applicationContext: Context,
    var onclickFromAdapter: OnclickFromAdapter,
) : RecyclerView.Adapter<MainAdapter.MyViewHolder>() {

    var context: Context = applicationContext

    override fun getItemCount(): Int = repoInfo.size

    @SuppressLint("NotifyDataSetChanged")
    fun addData(list: List<Items>) {
        repoInfo = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        Log.d("sizedd",repoInfo.size.toString())


        holder.repoName.text = repoInfo[position].fullName.toString()
        holder.repoDesc.text = repoInfo[position].description.toString()
        holder.starOfRepo.text = repoInfo[position].stargazersCount.toString()
        holder.watchersOfRepo.text = repoInfo[position].watchersCount.toString()
        holder.languages.text = repoInfo[position].language.toString()
        holder.contributors.text = repoInfo[position].forksCount.toString()

        val transformation = MultiTransformation(CenterCrop(), RoundedCorners(10))

        Glide.with(context)
            .load(repoInfo[position].owner?.avatarUrl)
            .transform(transformation)
            .into(holder.repoImage)

        holder.detailsButton.setOnClickListener {

           var list = mutableListOf<String>()

            onclickFromAdapter.details(
                repoInfo[position].fullName.toString(),
                repoInfo[position].description.toString(),
                repoInfo[position].owner?.avatarUrl.toString(),
                repoInfo[position].stargazersCount.toString(),
                repoInfo[position].owner?.reposUrl.toString(),
                repoInfo[position].homepage.toString()
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.single_item_view, parent, false)
        return MyViewHolder(view)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val repoName: TextView = itemView.findViewById(R.id.repoName)
        val repoImage: ImageView = itemView.findViewById(R.id.vinyl)
        val repoDesc: TextView = itemView.findViewById(R.id.repoDesc)
        val starOfRepo: TextView = itemView.findViewById(R.id.starOfRepo)
        val watchersOfRepo: TextView = itemView.findViewById(R.id.watchersOfRepo)
        val languages: TextView = itemView.findViewById(R.id.languages)
        val contributors: TextView = itemView.findViewById(R.id.contributors)
        val detailsButton: TextView = itemView.findViewById(R.id.detailsButton)


    }

}