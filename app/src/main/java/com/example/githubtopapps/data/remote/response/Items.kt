package com.example.githubtopapps.data.remote.response

import com.example.example.Owner
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Items(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("full_name") var fullName: String? = null,
    @SerializedName("private") var private: Boolean? = null,
    @SerializedName("owner") var owner: Owner? = Owner(),
    @SerializedName("description") var description: String? = null,
    @SerializedName("fork") var fork: Boolean? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("git_url") var gitUrl: String? = null,
    @SerializedName("ssh_url") var sshUrl: String? = null,
    @SerializedName("clone_url") var cloneUrl: String? = null,
    @SerializedName("svn_url") var svnUrl: String? = null,
    @SerializedName("homepage") var homepage: String? = null,
    @SerializedName("size") var size: Int? = null,
    @SerializedName("stargazers_count") var stargazersCount: Int? = null,
    @SerializedName("watchers_count") var watchersCount: Int? = null,
    @SerializedName("language") var language: String? = null,
    @SerializedName("has_issues") var hasIssues: Boolean? = null,
    @SerializedName("has_wiki") var hasWiki: Boolean? = null,
    @SerializedName("forks_count") var forksCount: Int? = null,
    @SerializedName("open_issues_count") var openIssuesCount: Int? = null,
    @SerializedName("allow_forking") var allowForking: Boolean? = null,
    @SerializedName("is_template") var isTemplate: Boolean? = null,
    @SerializedName("topics") var topics: ArrayList<String> = arrayListOf(),
    @SerializedName("visibility") var visibility: String? = null,
    @SerializedName("forks") var forks: Int? = null,
    @SerializedName("open_issues") var openIssues: Int? = null,
    @SerializedName("watchers") var watchers: Int? = null,
    @SerializedName("default_branch") var defaultBranch: String? = null,
    @SerializedName("score") var score: Int? = null

) :Serializable