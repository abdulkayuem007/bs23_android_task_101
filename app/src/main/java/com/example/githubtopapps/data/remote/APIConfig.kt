package com.example.githubtopapps.data.remote

import android.content.Context
import com.example.githubtopapps.R
import com.example.githubtopapps.data.local.pref.SharedPrefs
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIConfig {
    fun createRetrofitClient(context: Context): APIService {
        val sharedPrefs = SharedPrefs()
        sharedPrefs.init(context)
        // Will be used later
//        val token = sharedPrefs.read("userToken_Key", "").toString()

        val httpClient = OkHttpClient.Builder()
            .callTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)


        return Retrofit.Builder()
            .baseUrl(context.getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(APIService::class.java)
    }
}
