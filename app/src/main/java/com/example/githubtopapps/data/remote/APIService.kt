package com.example.githubtopapps.data.remote

import com.example.githubtopapps.data.remote.response.GithubResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface APIService {

    @Headers("Content-Type: application/json")
    @GET(EndPoints.SEARCH_REPO)
    fun getGithubInfo(
        @Query("q") q: String,
        @Query("sort") sort: String,
        @Query("order") order: String,
        @Query("per_page") per_page: String,
    ): Call<GithubResponse>

}
