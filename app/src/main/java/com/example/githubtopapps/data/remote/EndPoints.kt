package com.example.githubtopapps.data.remote

object EndPoints {
    const val SEARCH_REPO = "search/repositories"
}
