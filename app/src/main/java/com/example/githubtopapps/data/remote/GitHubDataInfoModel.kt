package com.example.githubtopapps.data.remote

import com.example.githubtopapps.data.common.RequestCompleteListener
import com.example.githubtopapps.data.remote.response.GithubResponse


interface GitHubDataInfoModel {
    fun getGitHubRepoInfo(topics:String,sort:String,order:String,per_page:String, callback: RequestCompleteListener<GithubResponse>)
}