package com.example.githubtopapps.data.remote

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.example.githubtopapps.data.common.RequestCompleteListener
import com.example.githubtopapps.data.remote.response.GithubResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GithubDataInfoModelShowModelImpl(private val context: Context): GitHubDataInfoModel {

    override fun getGitHubRepoInfo(
        topics: String,
        sort: String,
        order: String,
        per_page: String,
        callback: RequestCompleteListener<GithubResponse>
    ) {
        val apiService: APIService =  APIConfig.createRetrofitClient(context)

        apiService.getGithubInfo(
            topics,
            sort,
            order,
            per_page,
        ).enqueue(object : Callback<GithubResponse?> {
            override fun onResponse(call: Call<GithubResponse?>, response: Response<GithubResponse?>) {


                Log.d("responseFromService", Gson().toJson(response.body()))
                Log.d("responseFromService", Gson().toJson(call.request()))

                if(response.isSuccessful){
                    Log.d("responseGithub", Gson().toJson(response.body()))
                    response.body()?.let { callback.onRequestSuccess(it) }
                }
                else{
                    callback.onRequestFailed(response.message())
                }

            }

            override fun onFailure(call: Call<GithubResponse?>, t: Throwable) {
                callback.onRequestFailed(t.toString())

            }
        })

    }
}